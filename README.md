# Eleven
[![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat
            )](http://mit-license.org)
[![Platform](http://img.shields.io/badge/platform-ios%20%7C%20osx-lightgrey.svg?style=flat
             )](https://developer.apple.com/resources/)
![Language](http://img.shields.io/badge/language-objectivec-orange.svg?style=flat
             )
           
Eleven video player

