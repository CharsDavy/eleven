//
//  EMoreViewController.m
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import "EMoreViewController.h"
#import "ENavigationController.h"

@interface EMoreViewController ()

@end

@implementation EMoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = NSLocalizedString(@"More", nil);
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:(ENavigationController *)self.navigationController
                                                                            action:@selector(showMenu)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth - 310) / 2, 150, 310, 150)];
    [self.view addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = YiTextGray;
    label.numberOfLines = 0;
    label.text = @"Eleven是“十一”,“十一”二字取自“德”字。，由彳(chi)、十、目、一、心组成。\n十，指代直线，正确的标的方向。一，惟初太始。道立于一，造分天地，化成万物。\n\n";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
