//
//  EFileModel.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EFileModel : NSObject

@property (nonatomic, assign) long fileSize;
@property (nonatomic, strong) NSString *fileModificationDate;
@property (nonatomic, strong) NSString *fileCreationDate;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *fileType;

@end
