//
//  ELocalFileViewModel.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELocalFileViewModel : NSObject

- (NSArray *)getLocalVideoFiles;

- (NSArray *)getLocalFiles;

- (BOOL)removeLocalFile:(NSString *)localFile;

@end
