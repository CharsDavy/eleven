//
//  EWIFIManager.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPServer.h"

@interface EWIFIManager : NSObject

@property (nonatomic, strong) HTTPServer *httpServer;
@property (nonatomic, assign) BOOL serverStatus;

+ (instancetype)sharedInstance;
- (void)operateServer:(BOOL)status;

@end
