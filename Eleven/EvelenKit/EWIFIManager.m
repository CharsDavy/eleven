//
//  EWIFIManager.m
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import "EWIFIManager.h"

@implementation EWIFIManager

- (void)dealloc
{
    _httpServer.fileResourceDelegate = nil;
}

+ (instancetype)sharedInstance
{
    static EWIFIManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[EWIFIManager alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _httpServer = [[HTTPServer alloc] init];
        [_httpServer setType:@"_http._tcp."];
        [_httpServer setPort:8080];
        [_httpServer setName:@"CocoaWebResource"];
        [_httpServer setupBuiltInDocroot];
    }
    return self;
}

- (void)operateServer:(BOOL)status
{
    NSError *error;
    if (status) {
        BOOL serverIsRunning = [_httpServer start:&error];
        if (!serverIsRunning) {
            NSLog(@"Error starting HTTP Server: %@", error);
        }
    } else {
        [_httpServer stop];
    }
}

@end
