//
//  NSObject+IsNull.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (IsNull)

- (BOOL)isNull;

@end
