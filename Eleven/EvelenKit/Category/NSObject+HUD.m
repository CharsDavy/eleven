//
//  NSObject+HUD.m
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import "NSObject+HUD.h"

UIAlertView *alertView;

@implementation NSObject (HUD)

- (void)showProgressHUD:(NSString *)title afterDelay:(NSTimeInterval)delay
{
    [self showProgressHUD:title];
    [NSTimer scheduledTimerWithTimeInterval:delay
                                     target:self
                                   selector:@selector(hideProgressHUD:)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)showProgressHUD:(NSString *)title
{
    alertView = [[UIAlertView alloc] initWithTitle:@""
                                           message:title
                                          delegate:nil
                                 cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)hideProgressHUD
{
    [self hideProgressHUD:nil];
}

- (void)hideProgressHUD:(NSTimer *)timer
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

@end
