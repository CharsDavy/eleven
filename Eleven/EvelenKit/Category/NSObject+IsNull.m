//
//  NSObject+IsNull.m
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import "NSObject+IsNull.h"

@implementation NSObject (IsNull)

//判断对象是否为空
- (BOOL)isNull
{
    if ([self isEqual:[NSNull null]]) {
        return YES;
    } else {
        if ([self isKindOfClass:[NSNull class]]) {
            return YES;
        } else {
            if (self == nil) {
                return YES;
            }
        }
    }
    if ([self isKindOfClass:[NSString class]]) {
        if ([((NSString *)self) isEqualToString:@"(null)"]) {
            return YES;
        }
    }
    return NO;
}

@end
