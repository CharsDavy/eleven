//
//  NSObject+HUD.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (HUD)

- (void)showProgressHUD:(NSString *)title afterDelay:(NSTimeInterval)delay;
- (void)showProgressHUD:(NSString *)title;
- (void)hideProgressHUD;

@end
