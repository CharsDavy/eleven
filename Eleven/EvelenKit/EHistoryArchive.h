//
//  EHistoryArchive.h
//  Eleven
//
//  Created by dengw on 16/7/9.
//  Copyright © 2016年 chars. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EHistoryArchive : NSObject

- (void)saveArray:(NSArray *)array;//保存数组与归档
- (NSArray *)loadArchives;//解档得到数组

@end
